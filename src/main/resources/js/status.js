AJS.$(function () {
    var $ = jQuery;

    $('body').bind('ajaxComplete', function (e, x) {
        if (x.responseText && /user\-hover\-info/.test(x.responseText)) {
            var vcards = $('.user-hover-info');
            $.each(vcards, function (i, vcard) {
                var vcard = $(vcard),
                    email = $('h5 a', vcard).html();
                if (!vcard.hasClass('hc-status-applied')) {
                    $.get(AJS.$('meta[name="ajs-context-path"]').attr('content')
                        + '/rest/hipchatproxy/1/user/show?user_id=' + encodeURIComponent(email), function (d) {
                        vcard.addClass('hc-status-applied');
                        $('h4', vcard)
                            .append('<a class="hc-status ' + d.user.status.toLowerCase() + '" title="'
                            + d.user.status + ' on HipChat"><span>'
                            + d.user.status + '</span></a>');
                    }, 'json');
                }
            });

        }
    });

});