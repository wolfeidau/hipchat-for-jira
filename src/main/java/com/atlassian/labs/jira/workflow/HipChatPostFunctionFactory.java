package com.atlassian.labs.jira.workflow;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.labs.hipchat.HipChatProxyClient;
import com.atlassian.labs.hipchat.InvalidAuthTokenException;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import java.util.HashMap;
import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class HipChatPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {
    private static final Logger log = LoggerFactory.getLogger(HipChatPostFunction.class);
    public static final String ROOMS_TO_NOTIFY_JSON = "roomsToNotifyStr";
    public static final String ROOM_NAMES = "roomNames";
    public static final String ROOMS_JSON = "roomsJson";
    private final WorkflowManager workflowManager;
    private final HipChatProxyClient hipChatProxyClient;
    public static final String JQL_FIELD = "jql";


    public HipChatPostFunctionFactory(WorkflowManager workflowManager, HipChatProxyClient hipChatProxyClient) {
        this.workflowManager = workflowManager;
        this.hipChatProxyClient = hipChatProxyClient;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {

        Map<String, String[]> myParams = ActionContext.getParameters();

        final JiraWorkflow jiraWorkflow = workflowManager.getWorkflow(myParams.get("workflowName")[0]);

        //the default message
        velocityParams.put(ROOMS_TO_NOTIFY_JSON, "");
        velocityParams.put(ROOM_NAMES, "");
        velocityParams.put(JQL_FIELD, "");
        try {
            velocityParams.put(ROOMS_JSON, hipChatProxyClient.getRooms());
        } catch (InvalidAuthTokenException e) {
            log.error(e.getMessage());
        }

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {

        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;

        String roomsToNotify = (String) functionDescriptor.getArgs().get(ROOMS_TO_NOTIFY_JSON);
        String roomNames = (String) functionDescriptor.getArgs().get(ROOM_NAMES);

        if (roomsToNotify == null) {
            roomsToNotify = "";
        }

        String jql = (String) functionDescriptor.getArgs().get(JQL_FIELD);

        if (jql == null) {
            jql = "";
        }

        velocityParams.put(ROOMS_TO_NOTIFY_JSON, roomsToNotify);
        velocityParams.put(ROOM_NAMES, roomNames);
        velocityParams.put(JQL_FIELD, jql);
        try {
            velocityParams.put(ROOMS_JSON, hipChatProxyClient.getRooms());
        } catch (InvalidAuthTokenException e) {
            log.error(e.getMessage());
        }
    }


    public Map<String, ?> getDescriptorParams(Map<String, Object> formParams) {
        Map params = new HashMap();

        String roomsToNotify = extractSingleParam(formParams, ROOMS_TO_NOTIFY_JSON);
        params.remove(ROOMS_TO_NOTIFY_JSON);
        params.put(ROOMS_TO_NOTIFY_JSON, roomsToNotify);

        String roomNames = extractSingleParam(formParams, ROOM_NAMES);
        params.remove(ROOM_NAMES);
        params.put(ROOM_NAMES, roomNames);

        String jql = extractSingleParam(formParams, JQL_FIELD);
        params.put(JQL_FIELD, jql);
        
        return params;
    }

}