package com.atlassian.labs.hipchat.entities;

import java.util.ArrayList;
import java.util.List;

public class RoomsWrapper {
    private List<Room> rooms = new ArrayList<Room>();

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
