package com.atlassian.labs.hipchat.servlet;

import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class HipChatAuthTokenAdminServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(HipChatAuthTokenAdminServlet.class);
    private static final String ADMIN_TEMPLATE = "/templates/admin.vm";
    private final UserManager userManager;
    private final TemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;
    private final ConfigurationManager configurationManager;

    public HipChatAuthTokenAdminServlet(UserManager userManager, TemplateRenderer renderer,
                                        LoginUriProvider loginUriProvider, ConfigurationManager configurationManager) {
        this.userManager = checkNotNull(userManager, "userManager");
        this.renderer = checkNotNull(renderer, "renderer");
        this.loginUriProvider = checkNotNull(loginUriProvider, "loginUriProvider");
        this.configurationManager = checkNotNull(configurationManager, "configurationManager");
    }

    @Override
    protected final void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = userManager.getRemoteUsername(req);
        if (username == null || !userManager.isAdmin(username)) {
            redirectToLogin(req, resp);
            return;
        }

        resp.setContentType("text/html;charset=utf-8");
        Map<String, Object> context = Maps.newHashMap();
        context.put("hipChatAuthToken", configurationManager.getHipChatAuthToken());
        context.put("notifyRoomMatchingProjectKeyOnly", configurationManager.getNotifyRoomMatchingProjectKeyOnly());
        renderer.render(ADMIN_TEMPLATE, context, resp.getWriter());
    }

    @Override
    protected final void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = userManager.getRemoteUsername(req);
        if (username == null || !userManager.isAdmin(username)) {
            redirectToLogin(req, resp);
            return;
        }

        configurationManager.updateConfiguration(req.getParameter("hipChatAuthToken"), req.getParameter("notifyRoomMatchingProjectKeyOnly"));

        resp.setContentType("text/html;charset=utf-8");
        Map<String, Object> context = Maps.newHashMap();
        context.put("hipChatAuthToken", configurationManager.getHipChatAuthToken());
        context.put("notifyRoomMatchingProjectKeyOnly", configurationManager.getNotifyRoomMatchingProjectKeyOnly());
        renderer.render(ADMIN_TEMPLATE, context, resp.getWriter());
    }


    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

}