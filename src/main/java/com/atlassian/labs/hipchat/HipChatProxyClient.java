package com.atlassian.labs.hipchat;

import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.sal.api.net.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HipChatProxyClient
{
    private static final Logger log = LoggerFactory.getLogger("atlassian.plugin");
    private static String API_BASE_URI = "https://api.hipchat.com";

    private ConfigurationManager configurationManager;
    private RequestFactory<Request<?, Response>> requestFactory;

    public HipChatProxyClient(ConfigurationManager configurationManager, RequestFactory<Request<?, Response>> requestFactory)
    {
        this.configurationManager = configurationManager;
        this.requestFactory = requestFactory;
    }

    public String getUser(String userId)
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (StringUtils.isEmpty(authToken)) {
            return "";
        }
        final String url = API_BASE_URI + "/v1/users/show?auth_token=" + authToken + "&user_id=" + userId;
        final Request<?, Response> request = requestFactory.createRequest(Request.MethodType.GET, url);
        try {
            return request.executeAndReturn(new ResponseBodyReturningHandler());
        } catch (ResponseException e) {
            log.error("Failed to retrieve user", e);
        }
        return "";
    }

    public String getRooms() throws InvalidAuthTokenException
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (StringUtils.isEmpty(authToken)) {
            return "";
        }
        final String url = API_BASE_URI + "/v1/rooms/list?auth_token=" + authToken;
        final Request<?,Response> request = requestFactory.createRequest(Request.MethodType.GET, url);
        try {
            return request.executeAndReturn(new ResponseBodyReturningHandler());
        } catch (ResponseException e) {
            log.error("Failed to retrieve rooms", e);
        }
        return "";
    }

    public void notifyRoom(String room, String msg)
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (StringUtils.isEmpty(authToken)){
            return;
        }
        final String url = API_BASE_URI + "/v1/rooms/message?auth_token=" + authToken;
        final Request<?, Response> request = requestFactory.createRequest(Request.MethodType.POST, url);
        request.addRequestParameters(
                "room_id", room,
                "from", "JIRA",
                "message",  msg,
                "format", "json"
        );
        try {
            request.executeAndReturn(new ResponseBodyReturningHandler());
        } catch (ResponseException e) {
            log.error("Failed to notify rooms", e);
        }
    }

    private class ResponseBodyReturningHandler implements ReturningResponseHandler<Response, String>
    {
        @Override
        public String handle(Response response) throws ResponseException
        {
            return response.getResponseBodyAsString();
        }
    }
}