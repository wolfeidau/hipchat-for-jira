package com.atlassian.labs.hipchat.components;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.LoggerFactory;

public class ConfigurationManager {
    private static final String PLUGIN_STORAGE_KEY = "com.atlassian.labs.hipchat";
    private static final String HIPCHAT_AUTH_TOKEN_KEY = "hipchat-auth-token";
    private static final String NOTIFY_ROOM_MATCHING_PROJECT_ONLY_KEY = "notify-room-matching-project-only";

    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfigurationManager(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getHipChatAuthToken() {
        return getValue(HIPCHAT_AUTH_TOKEN_KEY);
    }

    public String getNotifyRoomMatchingProjectKeyOnly(){
        return getValue(NOTIFY_ROOM_MATCHING_PROJECT_ONLY_KEY);
    }
    
    public String getHipChatRooms(String spaceKey){
        return getValue(spaceKey);
    }

    private String getValue(String storageKey) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        Object storedValue = settings.get(storageKey);
        return storedValue == null ? "" : storedValue.toString();
    }

    public void updateConfiguration(String authToken, String notifyRoomMatchingProjectKeyOnly) {
        LoggerFactory.getLogger(this.getClass()).warn("updateConfiguration {} {}", authToken, notifyRoomMatchingProjectKeyOnly);
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(HIPCHAT_AUTH_TOKEN_KEY, authToken);
        settings.put(NOTIFY_ROOM_MATCHING_PROJECT_ONLY_KEY, notifyRoomMatchingProjectKeyOnly);
    }

    public void setNotifyRooms(String spaceKey, String rooms) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(spaceKey, rooms);
    }


}