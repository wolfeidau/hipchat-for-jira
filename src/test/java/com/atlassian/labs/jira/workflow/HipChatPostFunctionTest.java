package com.atlassian.labs.jira.workflow;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.labs.hipchat.HipChatProxyClient;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.labs.hipchat.entities.RoomsWrapper;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HipChatPostFunctionTest
{
    public static final String MESSAGE = "my message";

    protected HipChatPostFunction hipChatPostFunction;
    protected HipChatProxyClient hipChatProxyClient;

    protected MutableIssue issue;

    public static final String roomsJson = "{\"rooms\":[" +
            "{\"room_id\":123,\"name\":\"Canberra\",\"topic\":\"\",\"last_active\":1339551124,\"created\":1339550315,\"owner_user_id\":123,\"is_archived\":false,\"is_private\":true,\"xmpp_jid\":\"123_canberra@conf.hipchat.com\"}," +
            "{\"room_id\":456,\"name\":\"Melbourne\",\"topic\":\"\",\"last_active\":1355982079,\"created\":1335998875,\"owner_user_id\":123,\"is_archived\":false,\"is_private\":false,\"xmpp_jid\":\"456_melbourne@conf.hipchat.com\"}," +
            "{\"room_id\":789,\"name\":\"Sydney\",\"topic\":\"\",\"last_active\":1339550175,\"created\":1337659912,\"owner_user_id\":123,\"is_archived\":false,\"is_private\":true,\"xmpp_jid\":\"789_sydney@conf.hipchat.com\"}" +
            "]}";

    @Before
    public void before(){

        ConfigurationManager configurationManager = mock(ConfigurationManager.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        SearchService searchService = mock(SearchService.class);
        hipChatProxyClient = mock(HipChatProxyClient.class);
        hipChatPostFunction = new HipChatPostFunction(configurationManager, applicationProperties, searchService, hipChatProxyClient);
    }

    @Test
    public void testJsonParse() throws Exception {

        when(hipChatProxyClient.getRooms()).thenReturn(roomsJson);

        RoomsWrapper roomsWrapper = hipChatPostFunction.getRooms();

        assertNotNull(roomsWrapper);
    }


//    @Before
//    public void setup(ConfigurationManager configurationManager, WebResourceUrlProvider webResourceUrlProvider) {
//
//        issue = createPartialMockedIssue();
//        issue.setDescription("");
//
//        function = new HipChatPostFunction(configurationManager,webResourceUrlProvider) {
//            protected MutableIssue getIssue(Map transientVars) throws DataAccessException {
//                return issue;
//            }
//        };
//    }
//
//    @Test
//    public void testNullMessage() throws Exception
//    {
//        Map transientVars = Collections.emptyMap();
//
//        function.execute(transientVars,null,null);
//
//        assertEquals("message should be empty","",issue.getDescription());
//    }
//
//    @Test
//    public void testEmptyMessage() throws Exception
//    {
//        Map transientVars = new HashMap();
//        transientVars.put("messageField","");
//        function.execute(transientVars,null,null);
//
//        assertEquals("message should be empty","",issue.getDescription());
//    }
//
//    @Test
//    public void testValidMessage() throws Exception
//    {
//        Map transientVars = new HashMap();
//        transientVars.put("messageField",MESSAGE);
//        function.execute(transientVars,null,null);
//
//        assertEquals("message not found",MESSAGE,issue.getDescription());
//    }

//    private MutableIssue createPartialMockedIssue() {
//        GenericValue genericValue = mock(GenericValue.class);
//        IssueManager issueManager = mock(IssueManager.class);
//        ProjectManager projectManager = mock(ProjectManager.class);
//        VersionManager versionManager = mock(VersionManager.class);
//        IssueSecurityLevelManager issueSecurityLevelManager = mock(IssueSecurityLevelManager.class);
//        ConstantsManager constantsManager = mock(ConstantsManager.class);
//        SubTaskManager subTaskManager = mock(SubTaskManager.class);
//        AttachmentManager attachmentManager = mock(AttachmentManager.class);
//        LabelManager labelManager = mock(LabelManager.class);
//        ProjectComponentManager projectComponentManager = mock(ProjectComponentManager.class);
//        UserManager userManager = mock(UserManager.class);
//
//        return new IssueImpl(genericValue, issueManager, projectManager, versionManager, issueSecurityLevelManager, constantsManager, subTaskManager, attachmentManager, labelManager, projectComponentManager, userManager);
//    }

}
